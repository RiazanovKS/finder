package ru.rks.Finder;

import java.io.*;
import java.util.regex.*;

/**
 * Класс для поиска идентификаторов из файла с расширением .java
 *
 * @author Ryazanov K.S.
 */
public class Search {
    public static void main(String[] args) throws IOException {
        String string;
        Pattern p = Pattern.compile(".+");
        Matcher matcher = p.matcher("");

        Pattern spaces = Pattern.compile("\\s?\\S+");
        Matcher spacesFinder = spaces.matcher("");

        Pattern id = Pattern.compile("[A-Za-z_]+\\w*");
        Matcher idFinder = id.matcher("");

        Pattern commentPattern = Pattern.compile("\\/\\*\\*.+?\\*\\/");
        Matcher commentMatcher = commentPattern.matcher("");

        Pattern patternQuotes = Pattern.compile("\\\".+?\\\"");
        Matcher matcherQuotes = patternQuotes.matcher("");
/**
 * Записывает всю порграмму  из файла ManyFiles1 в 1 строку в файл 1string.txt
 */
        try (
                BufferedReader bufferedReader = new BufferedReader(new FileReader("D:\\work\\ManyFiles\\src\\ru\\rks\\ManyFiles1.java"));
                BufferedWriter writer = new BufferedWriter(new FileWriter("1string.txt"))) {
            while ((string = bufferedReader.readLine()) != null) {
                matcher.reset(string);

                while (matcher.find()) {
                    writer.write((matcher.group()));
                }
            }
        }

        /**
         * Поиск и замена коментариев и данных в ковычках на пустые строки , затем поиск идентификаторов и запись их в файл Identifiers.txt из файла 1string.txt .
         */
        try (
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("Identifiers.txt"));
                BufferedReader bufferedReader = new BufferedReader(new FileReader("1string.txt"))
        ) {
            while ((string = bufferedReader.readLine()) != null) {
                commentMatcher.reset(string);
                matcherQuotes.reset(string);
                while (commentMatcher.find() ) {
                    string = string.replaceAll("\\/\\*\\*.+?\\*\\/", "");
                }
                while(matcherQuotes.find()){
                    string = string.replaceAll("\".+?\"","");
                }
                idFinder.reset(string);
                while (idFinder.find()) {
                    bufferedWriter.write(idFinder.group() + "\n");
                }
            }
        }
        /**
         * Поиск и замена коментариев на пустые строки , затем запись программы без лишних пробельных символов в файл NewProgramm .java .
         */
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("1string.txt"));
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\work\\Finder\\src\\ru\\rks\\NewProgramm.java"))) {
            while ((string = bufferedReader.readLine()) != null) {
            string =string.replaceAll("ManyFiles1","NewProgramm");
                commentMatcher.reset(string);
                while (commentMatcher.find()) {
                    string = string.replaceAll("\\/\\*\\*.+?\\*\\/", "");
                }
                while (spacesFinder.find()) {
                    bufferedWriter.write(spacesFinder.group());
                }
                spacesFinder.reset(string);
            }
        }
    }
}




